import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.controllers.ThreadController;
import com.hw.db.models.Forum;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

public class ThreadControllerTests {
    private Thread thread;
    private Timestamp now;
    private ThreadController controller;
    private User user;
    private ArrayList<Post> testPosts;

    @BeforeEach
    @DisplayName("thread creation test")
    void createThreadTest() {
        now = Timestamp.valueOf(LocalDateTime.now());
        String nickname = "test author";
        user = new User(nickname, "author@gmail.com", "Name Surname", "Some info...");
        thread = new Thread(nickname, now, "forum", "message", "slug", "title", 0);
        int threadId = 0;
        thread.setId(threadId);
        controller = new ThreadController();
        testPosts = new ArrayList<Post>();
        testPosts.add(new Post(nickname, now, thread.getForum(), thread.getMessage(), 0, 0, false));
    }

    private void setUpThreadDaoMock(MockedStatic<ThreadDAO> threadDaoMock) {
        threadDaoMock
                .when(() -> ThreadDAO.getThreadBySlug(thread.getSlug()))
                .thenReturn(thread);
        threadDaoMock
                .when(() -> ThreadDAO.createPosts(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenAnswer((Answer<Void>) invocation -> null);
        threadDaoMock
                .when(() -> ThreadDAO.getPosts(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
                        Mockito.any()))
                .thenAnswer((Answer<List<Post>>) invocation -> testPosts);
    }

    private void setUpUserDaoMock(MockedStatic<UserDAO> userDaoMock) {
        userDaoMock
                .when(() -> UserDAO.Info(user.getNickname()))
                .thenReturn(user);
    }

    @Test
    @DisplayName("Correct post creation test")
    void correctlyCreatesPost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            // Arrange
            setUpThreadDaoMock(threadDaoMock);

            try (MockedStatic<UserDAO> userDaoMock = Mockito.mockStatic(UserDAO.class)) {
                setUpUserDaoMock(userDaoMock);

                // Act
                controller.createPost(thread.getSlug(), testPosts);

                // Assert
                threadDaoMock.verify(() -> ThreadDAO.createPosts(thread, testPosts, List.of(user)));
            }
        }
    }

    @Test
    @DisplayName("Correct get post")
    void correctlyGetsPosts() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            // Arrange
            setUpThreadDaoMock(threadDaoMock);

            // Act
            controller.Posts(thread.getSlug(), 1, 0, "", false);

            // Assert
            threadDaoMock.verify(() -> ThreadDAO.getPosts(thread.getId(), 1, 0, "", false));
        }
    }

    @Test
    @DisplayName("Correct change post")
    void correctlyChangesPost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            // Arrange
            setUpThreadDaoMock(threadDaoMock);

            // Act
            controller.change(thread.getSlug(), null);

            // Assert
            threadDaoMock.verify(() -> ThreadDAO.change(thread, null));
        }
    }

    @Test
    @DisplayName("Correct get info")
    void correctlyGetsInfo() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            // Arrange
            setUpThreadDaoMock(threadDaoMock);

            // Act
            controller.info(thread.getSlug());

            // Assert
            threadDaoMock.verify(() -> ThreadDAO.getThreadBySlug(thread.getSlug()));
        }
    }

    @Test
    @DisplayName("Correct create vote")
    void correctlyCreatesVote() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            // Arrange
            setUpThreadDaoMock(threadDaoMock);
            Vote testVote = new Vote(user.getNickname(), 0);

            try (MockedStatic<UserDAO> userDaoMock = Mockito.mockStatic(UserDAO.class)) {
                setUpUserDaoMock(userDaoMock);

                // Act
                controller.createVote(thread.getSlug(), testVote);

                // Assert
                threadDaoMock.verify(() -> ThreadDAO.createVote(thread, testVote));
            }
        }
    }

    @Test
    @DisplayName("Correct CheckIdOrSlug using ID")
    void correctlyChecksThreadById() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            // Arrange
            setUpThreadDaoMock(threadDaoMock);

            // Act
            controller.CheckIdOrSlug("0");

            // Assert
            threadDaoMock.verify(() -> ThreadDAO.getThreadById(0));
        }
    }

    @Test
    @DisplayName("Correct CheckIdOrSlug using slug")
    void correctlyChecksThreadBySlug() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            // Arrange
            setUpThreadDaoMock(threadDaoMock);

            // Act
            controller.CheckIdOrSlug("slug");

            // Assert
            threadDaoMock.verify(() -> ThreadDAO.getThreadBySlug("slug"));
        }
    }
}
